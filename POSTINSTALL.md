To configure your environment, open the terminal and access config.json in /app/data. You'll need to restart afterwards. Next, from terminal, run /app/code/influx CLI to create your databases, users, etc.
All data is stored in /app/data/influx
