FROM influxdb:1.8.6 as final

FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92 
COPY --from=final /usr/bin/influxd /app/code/influxd
COPY --from=final /usr/bin/influx /app/code/influx

ADD --chown=cloudron:cloudron start.sh config.json /app/code/

EXPOSE 8086
RUN mkdir -p /app/data && chown -R cloudron:cloudron /app/data && chmod +x /app/code/start.sh
WORKDIR /app/data
RUN mkdir -p /root/.influxdb && ln -s /app/data/influx/data /root/.influxdb && ln -s /app/data/influx/meta /root/.influxdb && ln -s /app/data/influx/wal /root/.influxdb && ln -s /app/data/.influxhistory /root/.influx_history
CMD [ "/app/code/start.sh" ]

